import os


# Elasticsearch settings
ES_HOST = os.getenv("ES_APIKEYS_HOST", os.getenv("ES_HOST", "localhost"))
ES_PORT = os.getenv("ES_APIKEYS_PORT", os.getenv("ES_PORT", 9200))
ES_USER = os.getenv("ES_APIKEYS_USER", os.getenv("ES_USER"))
ES_PWD = os.getenv("ES_APIKEYS_PWD", os.getenv("ES_PWD"))
# # API keys settings
ES_SYSTEM_INDEX = os.getenv("ES_SYSTEM_INDEX", "apikeys")

# # Header parameters
APIKEY = 'api-key'

MEMCACHED_HOST = os.getenv("MEMCACHED_HOST", "localhost")
MEMCACHED_PORT = int(os.getenv("MEMCACHED_PORT", 11211))
