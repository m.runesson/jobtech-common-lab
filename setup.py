from setuptools import setup, find_packages

setup(
    name='jobtech-common-lab',
    version='1.0.4',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'flask', 'flask-restx', 'flask-cors', 'elasticsearch~=7.6.0', 'certifi', 'pymemcache'
    ],
    tests_require=["pytest"]
)
